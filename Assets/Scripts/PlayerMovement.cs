﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float velocity;  //variable velocidad
    private float axis;   //eje
    public float limitx; //limitamos el movimiento en X.


	
	// Update is called once per frame
	void Update () {
        axis = Input.GetAxis("Horizontal");  //movimiento horizontal
        transform.Translate(velocity * axis * Time.deltaTime, 0, 0);
		
        if(transform.position.x > limitx)
        {
            transform.position = new Vector3(limitx, transform.position.y, transform.position.z);
        }else if(transform.position.x < -limitx)
        {
            transform.position = new Vector3(-limitx, transform.position.y, transform.position.z);
        }
	}
}
